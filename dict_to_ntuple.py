from collections import namedtuple


def dict_to_ntuple(d:dict, name:str="namedtuple"):
    """converts a dictionary into namedtuple"""
    assert isinstance(d, dict)
    return namedtuple(name, field_names=d.keys())(**d)
