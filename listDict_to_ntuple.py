from collections import namedtuple
from typing import Dict, List, NamedTuple, Tuple


def tuplelist(ld:List[Dict], name:str="row") -> Tuple[NamedTuple]:
    """converts a list of dictionaries to a tuple of namedtuples
    
    ## example

    >>> data = [{'x': 0, 'y': 5},
                {'x': 1, 'y': 8},
                {'x': 2, 'y': 11}]
    >>> record = tuplelist(data, "coords")
    >>> record
    (coords(x=0, y=5),
     coords(x=1, y=8),
     coords(x=2, y=11))
    """
    factory = namedtuple(name, field_names=tuple(ld[0].keys()))
    return tuple(factory(**d) for d in ld)
