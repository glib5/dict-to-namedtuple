from collections import namedtuple
from typing import NamedTuple, Tuple

import pandas as pd


def tupleframe(df:pd.DataFrame, name:str="row") -> Tuple[NamedTuple]:
    """converts a pandas DataFrame to a tuple of namedtuples
    ## example
    >>> data = pd.DataFrame({"x":[1, 2, 3], "y":[4, 5, 6]})
    >>> record = tupleframe(data, "coords")
    >>> record
    (coords(x=1, y=4),
     coords(x=2, y=5),
     coords(x=3, y=6))
    """
    factory = namedtuple(name, field_names=df.columns)
    return tuple(factory(**dict(row)) for _, row in df.iterrows())