from collections import namedtuple
from dataclasses import make_dataclass
from sys import getsizeof

def main():

    base = {"x":1, "y":2.2, "z":"zzz"}

    nt = namedtuple("nt", field_names=base.keys())
    base_nt = nt(**base)
    print(base_nt)
    print(getsizeof(base_nt)) # 64

    dc = make_dataclass("dc", fields=((k, type(v)) for k, v in base.items()), slots=True, frozen=True)
    base_dc = dc(**base)
    print(base_dc)
    print(getsizeof(base_dc)) # 56

    # same repr

    return 0

if __name__=="__main__":
    main()


